#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

NAME = "SetMeUp"
SMU_VERSION = "2.1"


def loadSetup():
    with open("setup.json") as json_data:
        return json.load(json_data)


def printModule(module_name, module):
    print("==== " + module_name + " ====")
    print(" Folder: " + module["folder"] + "/")
    print(" Comment: " + module["comment"])
    print(" Labels: " + str(module["labels"]))
    print(" Resourcen: ")
    for res in module["resources"]:
        print("  " + res)


jsonSetup = loadSetup()
modules_line = ""

for Module in jsonSetup["Modules"]:
    printModule(Module, jsonSetup["Modules"][Module])
    modules_line = modules_line + "[" + Module + "] "
    print("")

print(modules_line)

# TODO:
# - new module
# - add resource to module
# - edit module
# - - item by item
# - remove resource from module
# - print module
# - print module list
# - print commands
# - backup files from module definition
# - restore files from module definition
# - attach to system from module definition
# - commit changes
# - auto commit changes
# - 
#
